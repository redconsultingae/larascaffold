function initialState() {
    return {
        item: {
            id: null,
            type_of_use: null,
            transaction_type: null,
            number_bedrooms: null,
            number_bathrooms: null,
        },
        
        type_of_useEnum: [ { value: 'residential', label: 'Residential' }, { value: 'commercial', label: 'Commercial' }, ],
        transaction_typeEnum: [ { value: 'buy', label: 'Buy' }, { value: 'rent', label: 'Rent' }, ],
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    
    type_of_useEnum: state => state.type_of_useEnum,
    transaction_typeEnum: state => state.transaction_typeEnum,
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (! _.isEmpty(state.item.type_of_use) && typeof state.item.type_of_use === 'object') {
                params.set('type_of_use', state.item.type_of_use.value)
            }
            if (! _.isEmpty(state.item.transaction_type) && typeof state.item.transaction_type === 'object') {
                params.set('transaction_type', state.item.transaction_type.value)
            }

            axios.post('/api/v1/properties', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (! _.isEmpty(state.item.type_of_use) && typeof state.item.type_of_use === 'object') {
                params.set('type_of_use', state.item.type_of_use.value)
            }
            if (! _.isEmpty(state.item.transaction_type) && typeof state.item.transaction_type === 'object') {
                params.set('transaction_type', state.item.transaction_type.value)
            }

            axios.post('/api/v1/properties/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/properties/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        
    },
    
    setType_of_use({ commit }, value) {
        commit('setType_of_use', value)
    },
    setTransaction_type({ commit }, value) {
        commit('setTransaction_type', value)
    },
    setNumber_bedrooms({ commit }, value) {
        commit('setNumber_bedrooms', value)
    },
    setNumber_bathrooms({ commit }, value) {
        commit('setNumber_bathrooms', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setType_of_use(state, value) {
        state.item.type_of_use = value
    },
    setTransaction_type(state, value) {
        state.item.transaction_type = value
    },
    setNumber_bedrooms(state, value) {
        state.item.number_bedrooms = value
    },
    setNumber_bathrooms(state, value) {
        state.item.number_bathrooms = value
    },
    
    setType_of_useEnum(state, value) {
        state.type_of_useEnum = value
    },
    setTransaction_typeEnum(state, value) {
        state.transaction_typeEnum = value
    },
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
