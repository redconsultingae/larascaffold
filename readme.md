## LaraScaffold installation

### Please note: LaraScaffold requires fresh Laravel installation and is not suitable for use on already existing project.

1. Install the package via `composer require redconsulting/larascaffold`.
2. Run `php artisan larascaffold:install` and fill the required information.
3. Access LaraScaffold panel by visiting `http://yourdomain/larascaffold`.