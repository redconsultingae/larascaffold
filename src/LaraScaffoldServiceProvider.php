<?php

namespace RedConsulting\LaraScaffold;

use Illuminate\Support\ServiceProvider;
use RedConsulting\LaraScaffold\Console\LaraScaffoldInstall;

class LaraScaffoldServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/Translations', 'larascaffold');
        $this->loadRoutesFrom(__DIR__.'/routes.php');

        // Register vendor views
        $this->loadViewsFrom(__DIR__ . '/Views', 'ls');

        /* Publish master templates */
        /*$this->publishes([
            __DIR__ . '/Http/Kernel.php'                              => app_path('Http/Kernel.php'),
            __DIR__ . '/../publish/composer.json'                     => base_path('composer.json'),
            __DIR__ . '/../publish/package.json'                      => base_path('package.json'),
            __DIR__ . '/../publish/webpack.mix.js'                    => base_path('webpack.mix.js'),

            __DIR__ . '/../config/app.php'                            => config_path('app.php'),
            __DIR__ . '/../config/auth.php'                           => config_path('auth.php'),
            __DIR__ . '/../config/filesystems.php'                    => config_path('filesystems.php'),
            __DIR__ . '/Config/larascaffold.php'                      => config_path('larascaffold.php'),

            __DIR__ . '/../database/factories'                        => database_path('factories'),
            __DIR__ . '/../database/migrations'                       => database_path('migrations'),
            __DIR__ . '/../database/seeds'                            => database_path('seeds'),

            __DIR__ . '/../resources/views'                           => resource_path('views'),
            __DIR__ . '/../resources/client'                          => resource_path('client'),
            __DIR__ . '/../resources/lang/en/admin.php'               => resource_path('lang/en/admin.php'),

            __DIR__ . '/../public/themes/larascaffold'                => public_path('themes/larascaffold'),
            __DIR__ . '/../public/themes/admin'                       => public_path('themes/admin'),
            __DIR__ . '/../public/uploads'                            => public_path('uploads'),

            __DIR__ . '/../routes/web.php'                            => base_path('routes/web.php'),
            __DIR__ . '/../routes/api.php'                            => base_path('routes/api.php'),

            __DIR__ . '/Providers/AppServiceProvider.php'             => app_path('Providers/AppServiceProvider.php'),
            __DIR__ . '/Providers/AuthServiceProvider.php'            => app_path('Providers/AuthServiceProvider.php'),


            __DIR__ . '/Http/Controllers/publish/Api'                 => app_path('Http/Controllers/Api'),
            __DIR__ . '/Http/Controllers/publish/Traits'              => app_path('Http/Controllers/Traits'),
            __DIR__ . '/Http/Controllers/publish/FrontController.php' => app_path('Http/Controllers/FrontController.php'),
            __DIR__ . '/Http/Controllers/publish/HomeController.php'  => app_path('Http/Controllers/HomeController.php'),

            __DIR__ . '/Http/Middleware/RedirectIfAuthenticated.php'  => app_path('Http/Middleware/RedirectIfAuthenticated.php'),

            __DIR__ . '/Http/Requests'                                => app_path('Http/Requests'),
            __DIR__ . '/Http/Resources'                               => app_path('Http/Resources'),

            __DIR__ . '/Models/publish/Admin'                         => app_path('Admin.php'),
            __DIR__ . '/Models/publish/Permission'                    => app_path('Permission.php'),
            __DIR__ . '/Models/publish/Role'                          => app_path('Role.php'),
            __DIR__ . '/Models/publish/User'                          => app_path('User.php'),

        ], 'laraScaffold');*/


        if ($this->app->runningInConsole()) {
            $this->commands([
                LaraScaffoldInstall::class,
            ]);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Register main classes
        $this->app->make('RedConsulting\LaraScaffold\Http\Controllers\LaraScaffoldController');
        $this->app->make('RedConsulting\LaraScaffold\Http\Controllers\LaraScaffoldMenuController');
        $this->app->make('RedConsulting\LaraScaffold\Builders\MigrationBuilder');
        $this->app->make('RedConsulting\LaraScaffold\Builders\ModelBuilder');
        $this->app->make('RedConsulting\LaraScaffold\Builders\RequestBuilder');
        $this->app->make('RedConsulting\LaraScaffold\Builders\ControllerBuilder');
        $this->app->make('RedConsulting\LaraScaffold\Builders\ViewsBuilder');
//        $this->app->make('RedConsulting\LaraScaffold\Events\UserLoginEvents');
        // Register dependency packages
        $this->app->register('Collective\Html\HtmlServiceProvider');
        $this->app->register('Intervention\Image\ImageServiceProvider');
//        $this->app->register('Yajra\Datatables\DatatablesServiceProvider');

        // Register dependancy aliases
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('HTML', 'Collective\Html\HtmlFacade');
        $loader->alias('Form', 'Collective\Html\FormFacade');
        $loader->alias('Image', 'Intervention\Image\Facades\Image');
//        $loader->alias('Datatables', 'Yajra\Datatables\Datatables');
    }

}
