<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu"
            data-keep-expanded="false"
            data-auto-scroll="true"
            data-slide-speed="200">
            <li @if(Request::path() == config('larascaffold.route').'/menu') class="active" @endif>
                <a href="{{ url(config('larascaffold.route').'/menu') }}">
                    <i class="fa fa-list"></i>
                    <span class="title">{{ trans('larascaffold::admin.partials-sidebar-menu') }}</span>
                </a>
            </li>
            <li @if(Request::path() == config('larascaffold.route').'/install') class="active" @endif>
                <a href="#">
                    <i class="fa fa-download"></i>
                    <span class="title">Install</span>
                </a>
            </li>
        </ul>
    </div>
</div>
