@include('ls::partials.header')
@include('ls::partials.topbar')
<div class="clearfix"></div>
<div class="page-container">

    @include('ls::partials.sidebar')

    <div class="page-content-wrapper">
        <div class="page-content">

            <h3 class="page-title">
                Generator
            </h3>

            <div class="row">
                <div class="col-md-12">

                    @if (Session::has('message'))
                        <div class="note note-info">
                            <p>{{ Session::get('message') }}</p>
                        </div>
                    @endif

                     @yield('content')

                </div>
            </div>

        </div>
    </div>
</div>

<div class="scroll-to-top"
     style="display: none;">
    <i class="fa fa-arrow-up"></i>
</div>
@include('ls::partials.javascripts')

@yield('javascript')
@include('ls::partials.footer')


