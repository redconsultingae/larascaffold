<?php
namespace RedConsulting\LaraScaffold\Fields;

class FieldsDescriber
{
    /**
     * Default LaraScaffold field types
     * @return array
     */
    public static function types()
    {
        return [
            'text'         => 'Text',
            'email'        => 'Email',
            'textarea'     => 'Textarea',
            'radio'        => 'Radio',
            'checkbox'     => 'Checkbox',
            'date'         => 'Date',
            'datetime'     => 'Date and time picker',
            'time'         => 'Time',
            'relationship' => 'BelongsTo Relationship',
            'file'         => 'File',
            'photo'        => 'Photo',
            'password'     => 'Password',
            'money'        => 'Money',
            'number'       => 'Number',
            'float'        => 'Float',
            'enum'         => 'ENUM',
        ];
    }

    /**
     * Default LaraScaffold field validation types
     * @return array
     */
    public static function validation()
    {
        return [
            'optional'        => trans('larascaffold::strings.optional'),
            'required'        => trans('larascaffold::strings.required'),
            'required|unique' => trans('larascaffold::strings.required_unique')
        ];
    }

    /**
     * Set fields to be nullable by default if validation is not in this array
     * @return array
     */
    public static function nullables()
    {
        return [
            'optional',
        ];
    }

    /**
     * Default LaraScaffold field types for migration
     * @return array
     */
    public static function migration()
    {
        return [
            'text'         => 'string("$FIELDNAME$")',
            'email'        => 'string("$FIELDNAME$")',
            'textarea'     => 'text("$FIELDNAME$")',
            'radio'        => 'string("$FIELDNAME$")',
            'checkbox'     => 'tinyInteger("$FIELDNAME$")->default($STATE$)',
            'date'         => 'date("$FIELDNAME$")',
            'datetime'     => 'dateTime("$FIELDNAME$")',
            'time'         => 'time("$FIELDNAME$")',
            'relationship' => ['unsignedInteger("$RELATIONSHIP$_id")', 'foreign("$RELATIONSHIP$_id")->references("id")->on("$RELATIONSHIP$")'],
            'file'         => 'string("$FIELDNAME$")',
            'photo'        => 'string("$FIELDNAME$")',
            'password'     => 'string("$FIELDNAME$")',
            'money'        => 'decimal("$FIELDNAME$", 15, 2)',
            'number'       => 'integer("$FIELDNAME$")',
            'float'        => 'double("$FIELDNAME$", 15, 2)',
            'enum'         => 'enum("$FIELDNAME$", [$VALUES$])',
        ];
    }

    /**
     * Default LaraScaffold state for checkbox
     * @return array
     */
    public static function default_cbox()
    {
        return [
            'false' => trans('larascaffold::strings.default_unchecked'),
            'true'  => trans('larascaffold::strings.default_checked'),
        ];
    }
}