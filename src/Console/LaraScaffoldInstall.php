<?php

namespace RedConsulting\LaraScaffold\Console;

use App\User;
use Illuminate\Console\Command;
use RedConsulting\LaraScaffold\Models\Menu;
use RedConsulting\LaraScaffold\Models\Role;
use Symfony\Component\Process\Process;

class LaraScaffoldInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larascaffold:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run installation of LaraScaffold.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->info('Please note: LaraScaffold requires fresh Laravel installation!');
        $this->info('Starting installation process of LaraScaffold...');

        $this->info('1. Copying initial files');
        $this->copyInitial();

        $this->info('2. Copying master template to resource\views....');

        $this->callSilent('vendor:publish', [
            '--tag'   => ['laraScaffold'],
            '--force' => true
        ]);
        $this->info('Master template was transferred successfully');

        $this->info('2. Running migrations');
        $this->call('migrate');

        $this->info('3. Running seeders');
        $this->call('db:seed');

        Menu::insert([
            [
                'name'      => 'User',
                'title'     => 'User',
                'menu_type' => 0
            ],
            [
                'name'      => 'Role',
                'title'     => 'Role',
                'menu_type' => 0
            ]
        ]);



//        1. Extract the archive and put it in the folder you want
//
//        2. Prepare your .env file there with database connection and other settings
//
//        3. Run "composer install" command
//
//        4. Run "php artisan migrate --seed" command. Notice: seed is important, because it will create the first admin user for you.
//
//                                                                                                                        5. Run "php artisan key:generate" command.
//
//        6. Run "php artisan passport:install" command.
//
//        7. Run "npm install" command.
//
//        8. Run "npm run dev" command.

//        $this->info('4. Calling npm install and composer update');
//
//        $cmd = new Process("composer update");
//        $cmd->setWorkingDirectory(base_path());
//        $cmd->run();
//
//        $cmd = new Process("npm install");
//        $cmd->setWorkingDirectory(base_path());
//        $cmd->run();
//
//        $this->info('2. Running migrations');
//        $this->call('migrate');
//
//        $this->info('3. Running seeders');
//        $this->call('db:seed');
//
//        $this->info('Installation was successful.');
    }

    /**
     *  Copy migration files to database_path('migrations') and User.php model to App
     */
    public function copyInitial()
    {
        copy(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Migrations' . DIRECTORY_SEPARATOR . '2015_10_10_000000_create_menus_table',
            database_path('migrations' . DIRECTORY_SEPARATOR . '2015_10_10_000000_create_menus_table.php'));
        copy(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Migrations' . DIRECTORY_SEPARATOR . '2015_12_11_000000_create_users_logs_table',
            database_path('migrations' . DIRECTORY_SEPARATOR . '2015_12_11_000000_create_users_logs_table.php'));
        copy(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Migrations' . DIRECTORY_SEPARATOR . '2015_12_12_000000_create_menu_role_table',
            database_path('migrations' . DIRECTORY_SEPARATOR . '2015_12_12_000000_create_menu_role_table.php'));
        $this->info('Migrations were transferred successfully');
    }



    /**
     *  Copy master template to resource/view
     */
    public function copyMasterTemplate()
    {
        Menu::insert([
            [
                'name'      => 'User',
                'title'     => 'User',
                'menu_type' => 0
            ],
            [
                'name'      => 'Role',
                'title'     => 'Role',
                'menu_type' => 0
            ]
        ]);
        $this->callSilent('vendor:publish', [
            '--tag'   => ['laraScaffold'],
            '--force' => true
        ]);
        $this->info('Master template was transferred successfully');
    }
}

