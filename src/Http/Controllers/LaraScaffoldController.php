<?php
namespace RedConsulting\LaraScaffold\Http\Controllers;

use App\Http\Controllers\Controller;

class LaraScaffoldController extends Controller
{
    /**
     * Show LaraScaffold dashboard page
     *
     * @return Response
     */
    public function index()
    {
        return view('ls::dashboard');
    }
}