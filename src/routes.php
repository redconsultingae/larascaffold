<?php

/**
 * Package routing file specifies all of this package routes.
 */

Route::group([
    'namespace'  => 'RedConsulting\LaraScaffold\Http\Controllers',
    'middleware' => ['web']
], function () {
    // Dashboard home page route
    Route::get('larascaffold', 'LaraScaffoldController@index');
    //Route::get(config('larascaffold.homeRoute'), config('larascaffold.homeAction','LaraScaffoldController@index'));


    // Menu routing
    Route::get(config('larascaffold.route') . '/menu', [
        'as'   => 'menu',
        'uses' => 'LaraScaffoldMenuController@index'
    ]);
    Route::post(config('larascaffold.route') . '/menu', [
        'as'   => 'menu',
        'uses' => 'LaraScaffoldMenuController@rearrange'
    ]);

    Route::get(config('larascaffold.route') . '/menu/edit/{id}', [
        'as'   => 'menu.edit',
        'uses' => 'LaraScaffoldMenuController@edit'
    ]);
    Route::post(config('larascaffold.route') . '/menu/edit/{id}', [
        'as'   => 'menu.edit',
        'uses' => 'LaraScaffoldMenuController@update'
    ]);

    Route::get(config('larascaffold.route') . '/menu/crud', [
        'as'   => 'menu.crud',
        'uses' => 'LaraScaffoldMenuController@createCrud'
    ]);
    Route::get(config('larascaffold.route') . '/menu/crud/edit/{id}', [
        'as'   => 'menu.crud.edit',
        'uses' => 'LaraScaffoldMenuController@editCrud'
    ]);
    Route::post(config('larascaffold.route') . '/menu/crud', [
        'as'   => 'menu.crud.insert',
        'uses' => 'LaraScaffoldMenuController@insertCrud'
    ]);
    Route::post(config('larascaffold.route') . '/menu/crud/edit/{id}', [
        'as'   => 'menu.crud.edit',
        'uses' => 'LaraScaffoldMenuController@insertCrud'
    ]);

    Route::get(config('larascaffold.route') . '/menu/parent', [
        'as'   => 'menu.parent',
        'uses' => 'LaraScaffoldMenuController@createParent'
    ]);
    Route::post(config('larascaffold.route') . '/menu/parent', [
        'as'   => 'menu.parent.insert',
        'uses' => 'LaraScaffoldMenuController@insertParent'
    ]);

    Route::get(config('larascaffold.route') . '/menu/custom', [
        'as'   => 'menu.custom',
        'uses' => 'LaraScaffoldMenuController@createCustom'
    ]);
    Route::post(config('larascaffold.route') . '/menu/custom', [
        'as'   => 'menu.custom.insert',
        'uses' => 'LaraScaffoldMenuController@insertCustom'
    ]);
});
