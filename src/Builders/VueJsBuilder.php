<?php
namespace RedConsulting\LaraScaffold\Builders;

use Illuminate\Support\Str;
use RedConsulting\LaraScaffold\Models\Menu;

class VueJsBuilder
{
    // Templates
    private $componentsTemplate; // Array: [0]->index, [1]->edit, [2]->create, [3]->show
    private $storeTemplate; // Array: [0]->index, [1]->single
    // Variables
    private $fields;
    private $route;
    private $resource;
    private $path;
    private $model;
    private $files;
    private $relationships;
    // @todo Move into FieldsDescriber for usage in fields extension
    private $starred = [
        'required',
        'required|unique'
    ];


    /**
     * Build our views files
     */
    public function build($id)
    {
        $menu = Menu::findOrFail($id);
        $fieldsinfo = $menu->fieldsinfo;
        $this->componentsTemplate = [
            0 => __DIR__ . '/../Templates/vuejs/components/Index',
            1 => __DIR__ . '/../Templates/vuejs/components/Edit',
            2 => __DIR__ . '/../Templates/vuejs/components/Create',
            3 => __DIR__ . '/../Templates/vuejs/components/Show',
        ];
        $this->storeTemplate = [
            0 => __DIR__ . '/../Templates/vuejs/store/index',
            1 => __DIR__ . '/../Templates/vuejs/store/single',
        ];
        $this->name     = $fieldsinfo['name'];
        $this->fields   = $fieldsinfo['fields'];
        $this->files    = $fieldsinfo['files'];
        $this->enum     = $fieldsinfo['enum'];

        $this->names();

        list($componentsTemplate, $storeTemplate) = $this->loadTemplate();
        $componentsTemplate = $this->buildComponentsParts($componentsTemplate);
        $storeTemplate = $this->buildStoreParts($storeTemplate);

        $storeFile = $this->updateStoreFile();
        $routerFile = $this->updateRouterFile();

        $this->publish($componentsTemplate, $storeTemplate, $storeFile, $routerFile);
    }

    /**
     *  Load views templates
     */
    private function loadTemplate()
    {
        return [
            [
                0 => $this->componentsTemplate[0] != '' ? file_get_contents($this->componentsTemplate[0]) : '',
                1 => $this->componentsTemplate[1] != '' ? file_get_contents($this->componentsTemplate[1]) : '',
                2 => $this->componentsTemplate[2] != '' ? file_get_contents($this->componentsTemplate[2]) : '',
                3 => $this->componentsTemplate[3] != '' ? file_get_contents($this->componentsTemplate[3]) : '',
            ],
            [
                0 => $this->storeTemplate[0] != '' ? file_get_contents($this->storeTemplate[0]) : '',
                1 => $this->storeTemplate[1] != '' ? file_get_contents($this->storeTemplate[1]) : '',
            ]

        ];
    }

    /**
     * Build views templates parts
     *
     * @param $template
     *
     * @return mixed
     */
    private function buildComponentsParts($template)
    {
        // Index template
        $columns = $this->buildColumns();
        $store_name = $this->name . "Index";

        //dump($template[0]);

        $template[0] = str_replace([
            '$MODULE$',
            '$STORE_NAME$',
            '$COLUMNS$',
            '$ROUTE$',
        ], [
            $this->module,
            $store_name,
            $columns,
            $this->route,
        ], $template[0]);


        // Edit template
        $store_name = $this->name . "Single";

        $_fields_getters_array = ['item', 'loading'];
        if ($this->enum > 0) {
            foreach ($this->fields as $field) {
                if ($field['type'] == 'enum') {
                    array_push($_fields_getters_array, $field['title'].'Enum');
                }
            }
        }
        $fields_getters_array = json_encode($_fields_getters_array);

        $_fields_actions_array = ['fetchData', 'updateData', 'resetState'];
        foreach ($this->fields as $field) {
            array_push($_fields_actions_array, 'set' . ucfirst($field['title']));
        }
        $fields_actions_array = json_encode($_fields_actions_array);


        $fields_actions = '';
        foreach ($this->fields as $field) {
            if ($field['type'] == 'enum') {
                $fields_actions .= "update" . ucfirst($field['title']) . "(value) { this.set" . ucfirst($field['title']) . "(value) },\n";
            } else {
                $fields_actions .= "update" . ucfirst($field['title']) . "(e) { this.set" . ucfirst($field['title']) . "(e.target.value) },\n";
            }
        }

        $form_fields = $this->buildForm('edit');

        $template[1] = str_replace([
            '$MODULE$',
            '$ROUTE$',
            '$STORE_NAME$',
            '$FORMFIELDS$',
            '$FIELDS_GETTERS_ARRAY$',
            '$FIELDS_ACTIONS_ARRAY$',
            '$FIELDS_ACTIONS$',
        ], [
            $this->module,
            $this->route,
            $store_name,
            $form_fields,
            $fields_getters_array,
            $fields_actions_array,
            $fields_actions,
        ], $template[1]);


        // Create template
        $fetch_relationships = '';
        $_fields_getters_array = ['item', 'loading'];

        if ($this->enum > 0) {
            foreach ($this->fields as $field) {
                if ($field['type'] == 'enum') {
                    array_push($_fields_getters_array, $field['title'].'Enum');
                }
            }
        }
        $fields_getters_array = json_encode($_fields_getters_array);

        $_fields_actions_array = ['storeData', 'resetState'];
        foreach ($this->fields as $field) {
            array_push($_fields_actions_array, 'set' . ucfirst($field['title']));
        }
        $fields_actions_array = json_encode($_fields_actions_array);


        $fields_actions = '';
        foreach ($this->fields as $field) {
            if ($field['type'] == 'enum') {
                $fields_actions .= "update" . ucfirst($field['title']) . "(value) { this.set" . ucfirst($field['title']) . "(value) },\n";
            } else {
                $fields_actions .= "update" . ucfirst($field['title']) . "(e) { this.set" . ucfirst($field['title']) . "(e.target.value) },\n";
            }
        }

        $form_fields = $this->buildForm('create');
        $template[2] = str_replace([
            '$MODULE$',
            '$ROUTE$',
            '$STORE_NAME$',
            '$FORMFIELDS$',
            '$FETCH_RELATIONSHIPS$',
            '$FIELDS_GETTERS_ARRAY$',
            '$FIELDS_ACTIONS_ARRAY$',
            '$FIELDS_ACTIONS$',
        ], [
            $this->module,
            $this->route,
            $store_name,
            $form_fields,
            $fetch_relationships,
            $fields_getters_array,
            $fields_actions_array,
            $fields_actions,
        ], $template[2]);


        // Show template

        $fields_details = $this->buildShowDetails();

        $template[3] = str_replace([
            '$MODULE$',
            '$STORE_NAME$',
            '$FIELDS_DETAILS$'
        ], [
            $this->module,
            $store_name,
            $fields_details,
        ], $template[3]);

        return $template;
    }

    private function buildStoreParts($template)
    {
        // Index template
        $template[0] = str_replace([
            '$RELATIONSHIPS$',
            '$RESOURCE$',
        ], [
            $this->relationships,
            $this->resource
        ], $template[0]);

        // Single template
        $fields_init = "id: null,\n            ";
        $fields_types = '';
        $fields_getters = '';
        $fields_actions = '';
        $fields_mutations = '';
        $fields_custom_types = '';



        foreach ($this->fields as $field) {
            $fields_init .= (($field['type'] == 'relationship') ?  $field['relationship_name'] : $field['title'] ). ": null,\n            ";

            $fields_actions .= "set" . ucfirst($field['type'] == 'relationship' ? $field['relationship_name'] : $field['title']) . ($field['type'] == 'enum' ? 'Enum' : '') . "({ commit }, value) { commit('set" . ucfirst($field['type'] == 'relationship' ? $field['relationship_name'] : $field['title']) . "', value) },\n    ";
            $fields_mutations .= "set" . ucfirst($field['type'] == 'relationship' ? $field['relationship_name'] : $field['title']) . ($field['type'] == 'enum' ? 'Enum' : ''). "(state, value) { state.item." . ($field['type'] == 'relationship' ? $field['relationship_name'] : $field['title']) . " = value },\n    ";

            if ($field['type'] == 'relationship') {
                $fields_types .= $field['relationship_name'] . "All: [],\n        ";
                $fields_getters .=  $field['relationship_name'] . "All: state => state." . $field['relationship_name'] . "All,\n    ";

                $fields_custom_types .= "if (! _.isEmpty(state.item.". $field['relationship_name'] .")) {\n                params.set('". $field['relationship_name']. "_id', '')\n            } else {\n                params.set('". $field['relationship_name']. "_id', state.item.". $field['relationship_name']. ".id)\n            }\n        ";
            }

            if ($field['type'] == 'enum') {
                // Fields Getters
                $fields_getters .=  $field['title'] . "Enum: state => state." . $field['title'] . "Enum,\n    ";

                // Fields Custom Types
                $fields_custom_types .= "if (! _.isEmpty(state.item.". $field['title'] .") && typeof state.item.". $field['title'] ." === 'object') {\n                params.set('". $field['title']. "', state.item.". $field['title'] .".value)\n            }\n            ";

                // Fields Types
                $field['enum'] = explode(',', $field['enum']);
                $fields_types .= $field['title'] . "Enum: [ ";
                foreach($field['enum'] as $enum) {
                    $fields_types .= "{ value: '" . $enum . "', label: '" . ucfirst($enum) . "' }, ";
                }
                rtrim($fields_types, ", ");
                $fields_types .= "],\n        ";
            }

        }

        $template[1] = str_replace([
            '$RESOURCE$',
            '$FIELDS_INIT$',
            '$FIELDS_TYPES$',
            '$FIELDS_GETTERS$',
            '$FIELDS_ACTIONS$',
            '$FIELDS_MUTATIONS$',
            '$FIELDS_CUSTOM_TYPES$',
        ], [
            $this->resource,
            $fields_init,
            $fields_types,
            $fields_getters,
            $fields_actions,
            $fields_mutations,
            $fields_custom_types
        ], $template[1]);

        return $template;
    }

    /**
     *  Build index table
     */
    private function buildColumns()
    {
        // JSON ENCODE
        /*[{field: "id",
                title: "#",
                width: 50,
                sortable: false,
                textAlign: 'center',
                selector: {class: 'm-checkbox--solid m-checkbox--brand'}
            }, {
        field: "title",
                title: "Title",
                responsive: {visible: 'lg'}
            }],*/

        $columns = [];
        foreach ($this->fields as $field) {
            if ($field['type'] != 'password'
                && $field['type'] != 'textarea'
                && $field['show'] == 1
            ) {
//                if ($field['type'] == 'relationship') {
//                    $columns .= '<td>{{ isset($row->' . $field['relationship_name'] . '->' . $field['relationship_field'] . ') ? $row->' . $field['relationship_name'] . '->' . $field['relationship_field'] . " : '' }}</td>\r\n";
//                } elseif ($field['type'] == 'photo') {
//                    $columns .= '<td>@if($row->' . $field['title'] . ' != \'\')<img src="{{ asset(\'uploads/thumb\') . \'/\'.  $row->' . $field['title'] . " }}\">@endif</td>\r\n";
//                } else {
//                    $columns .= '<td>{{ $row->' . $field['title'] . " }}</td>\r\n";
//                }
                $column = [
                    'field' => $field['title'],
                    'title' => $field['label']
                ];
                array_push($columns, $column);
            }
        }

        return trim(json_encode($columns, JSON_PRETTY_PRINT), '[]');
    }

    private function buildShowDetails()
    {
        $table = '<table class="table table-bordered"><tbody>';

        foreach ($this->fields as $field) {
            $table .= '<tr><th>' . $field['label'] . '</th><td>{{ item.' . $field['title'] . ' }}</td></tr>';
        }

        $table .= '</tbody></table>';
        return $table;
    }

    /**
     *  Build edit.blade.php form
     */
    private function buildForm($type)
    {
        $form = '';
        foreach ($this->fields as $field) {
            $title = addslashes($field['label']);
            $label = $field['title'];
            if (in_array($field['validation'],
                    $this->starred) && $field['type'] != 'password' && $field['type'] != 'file' && $field['type'] != 'photo'
            ) {
                $title .= '*';
            }
            if ($field['type'] == 'relationship') {
                $label = $field['relationship_name'] . '_id';
            }
            if ($field['type'] == 'checkbox') {
                $field['default'] = '$' . $this->model . '->' . $label . ' == 1';
            }
            $temp = file_get_contents(__DIR__  . '/../Templates/fields/' . $field['type']);
            $temp = str_replace([
                '$LABEL$',
                '$TITLE$',
                '$VALUE$',
                '$UPDATETRIGGER$',
                '$ENUMOPTIONS$',
                '$STATE$',
                '$SELECT$',
                '$TEXTEDITOR$',
                '$HELPER$',
                '$WIDTH$',
                '$HEIGHT$',
            ], [
                $label,
                $title,
                $field['type'] != 'radio' ?
                    $field['value'] != '' ? ', "' . $field['value'] . '"' : ''
                    : "'". $field['value']. "'",
                'update'.ucfirst($field['title']),
                $field['title'].'Enum',
                $field['default'],
                '$' . $field['relationship_name'],
                $field['texteditor'] == 1 ? ' ckeditor' : '',
                $this->helper($field['helper']),
                $field['dimension_w'],
                $field['dimension_h'],
            ], $temp);
            $form .= $temp;
        }
        return $form;
    }

    public function updateStoreFile() {
        $menus = Menu::whereNotIn('menu_type',[0,2])->get();

        $dec = '';
        $call = '';
        foreach ($menus as $menu) {
            $dec .= "import " . $menu->name . "Index from './modules/" . $menu->name . "'\n";
            $dec .= "import " . $menu->name . "Single from './modules/" . $menu->name . "/single'\n";

            $call .= "        " .$menu->name . "Index,\n";
            $call .= "        " . $menu->name . "Single,\n";
        }

        $template = file_get_contents(base_path('resources/client/assets/js/store/index.js'));
        $patterns = [
            "/([\s\S]*\/\/ ~~start~~ \[DEC\] Do not remove this comment\n)([\s\S]*?)(\/\/ ~~end~~ \[DEC\] Do not remove this comment[\s\S]*)/",
            "/([\s\S]*\/\/ ~~start~~ \[CALL\] Do not remove this comment\n)([\s\S]*?)(\/\/ ~~end~~ \[CALL\] Do not remove this comment[\s\S]*)/",
        ];
        $replacements = [
            "$1".$dec."$3",
            "$1".$call."        $3",
        ];

        return preg_replace($patterns, $replacements, $template);
    }

    public function updateRouterFile() {
        $menus = Menu::whereNotIn('menu_type', [0,2])->get();

        $dec = '';
        $call = '';
        foreach ($menus as $menu) {
            $dec .= "import " . $menu->name . "Index from '../components/cruds/" . $menu->name . "/Index.vue'\n";
            $dec .= "import " . $menu->name . "Create from '../components/cruds/" . $menu->name . "/Create.vue'\n";
            $dec .= "import " . $menu->name . "Show from '../components/cruds/" . $menu->name . "/Show.vue'\n";
            $dec .= "import " . $menu->name . "Edit from '../components/cruds/" . $menu->name . "/Edit.vue'\n";

            $call .= "    { path: '/" . Str::snake($menu->name) . "', component: " . $menu->name . "Index, name: '" . Str::snake($menu->name) . ".index' },\n";
            $call .= "    { path: '/" . Str::snake($menu->name) . "/create', component: " . $menu->name . "Create, name: '" . Str::snake($menu->name) . ".create' },\n";
            $call .= "    { path: '/" . Str::snake($menu->name) . "/:id', component: " . $menu->name . "Show, name: '" . Str::snake($menu->name) . ".show' },\n";
            $call .= "    { path: '/" . Str::snake($menu->name) . "/:id/edit', component: " . $menu->name . "Edit, name: '" . Str::snake($menu->name) . ".edit' },\n";
        }

        $template = file_get_contents(base_path('resources/client/assets/js/router/index.js'));
        $patterns = [
            "/([\s\S]*\/\/ ~~start~~ \[DEC\] Do not remove this comment\n)([\s\S]*?)(\/\/ ~~end~~ \[DEC\] Do not remove this comment[\s\S]*)/",
            "/([\s\S]*\/\/ ~~start~~ \[CALL\] Do not remove this comment\n)([\s\S]*?)(\/\/ ~~end~~ \[CALL\] Do not remove this comment[\s\S]*)/",
        ];
        $replacements = [
            "$1".$dec."$3",
            "$1".$call."    $3",
        ];

        return preg_replace($patterns, $replacements, $template);
    }

    /**
     *  Generate names for the views
     */
    private function names()
    {
        $camelCase      = ucfirst(Str::camel($this->name));
        $this->route    = Str::snake($camelCase);
        $this->module   = $camelCase;
        $this->resource = Str::snake($camelCase);
        $this->model    = strtolower($camelCase);
        $this->path     = $camelCase;
    }

    /**
     * Create helper blocks for form fields
     *
     * @param $value
     *
     * @return string
     */
    private function helper($value)
    {
        if ($value != '') {
            return '<p class="help-block">' . $value . '</p>';
        } else {
            return '';
        }
    }

    /**
     *  Publish files into their places
     */
    private function publish($componentsTemplate, $storeTemplate, $storeFile, $routerFile)
    {
        if (! file_exists(base_path('resources/client/assets/js/components/cruds/' . $this->path))) {
            mkdir(base_path('resources/client/assets/js/components/cruds/' . $this->path));
            chmod(base_path('resources/client/assets/js/components/cruds'), 0777);
        }
        file_put_contents(base_path('resources/client/assets/js/components/cruds/' . $this->path  . '/Index.vue'), $componentsTemplate[0]);
        file_put_contents(base_path('resources/client/assets/js/components/cruds/' . $this->path  . '/Edit.vue'), $componentsTemplate[1]);
        file_put_contents(base_path('resources/client/assets/js/components/cruds/' . $this->path  . '/Create.vue'), $componentsTemplate[2]);
        file_put_contents(base_path('resources/client/assets/js/components/cruds/' . $this->path  . '/Show.vue'), $componentsTemplate[3]);

        if (! file_exists(base_path('resources/client/assets/js/store/modules/' . $this->path))) {
            mkdir(base_path('resources/client/assets/js/store/modules/' . $this->path));
            chmod(base_path('resources/client/assets/js/store/modules'), 0777);
        }
        file_put_contents(base_path('resources/client/assets/js/store/modules/' . $this->path  . '/index.js'), $storeTemplate[0]);
        file_put_contents(base_path('resources/client/assets/js/store/modules/' . $this->path  . '/single.js'), $storeTemplate[1]);

        file_put_contents(base_path('resources/client/assets/js/store/index.js'), $storeFile);
        file_put_contents(base_path('resources/client/assets/js/router/index.js'), $routerFile);
    }

}