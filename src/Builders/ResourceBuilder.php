<?php
namespace RedConsulting\LaraScaffold\Builders;

use Illuminate\Support\Str;
use RedConsulting\LaraScaffold\Models\Menu;

class ResourceBuilder
{
    // Request namespace
    private $namespace = 'App\Http\Resources';
    // Template
    private $template;
    // Names
    private $name;
    private $className;
    private $fileName;

    /**
     * Build our request file
     */
    public function build($id)
    {
        $menu = Menu::findOrFail($id);
        $fieldsinfo = $menu->fieldsinfo;
        $this->template = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Templates' . DIRECTORY_SEPARATOR . 'resource';
        $this->name     = $fieldsinfo['name'];
        $this->names();
        $template = (string)$this->loadTemplate();
        $template = $this->buildParts($template);
        $this->publish($template);
    }

    /**
     *  Load request template
     */
    private function loadTemplate()
    {
        return file_get_contents($this->template);
    }

    /**
     * Build request template parts
     *
     * @param $template
     *
     * @return mixed
     */
    private function buildParts($template)
    {
        $template = str_replace([
            '$NAMESPACE$',
            '$CLASS$'
        ], [
            $this->namespace,
            $this->className
        ], $template);

        return $template;
    }

    /**
     *  Generate file and class names for the request
     */
    private function names()
    {
        $camel           = ucfirst(Str::camel(Str::singular($this->name)));
        $this->className = $camel;

        $fileName       = $this->className . '.php';
        $this->fileName = $fileName;
    }

    /**
     *  Publish file into it's place
     */
    private function publish($template)
    {
        file_put_contents(app_path('Http/Resources/' . $this->fileName), $template);
    }

}